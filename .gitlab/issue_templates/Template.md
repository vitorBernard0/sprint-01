## Issue:
[Descrição breve e direta do problema]

## Descrição:
[Descição de forma detalhada, informações relevantes, comportamento esperado e observado]

## Como Reproduzir:
[Lista de passos de como chegar até aquele comportamento ou falha]

1. 
2. 
3. 
...


## Funcionamento Esperado:
[Descrição de qual comportamento era esperado]


## Comportamento Observado:
[Descrição do comportamento observado e que está em desacordo com o comportamento esperado.]


## Ambiente de Teste:
[Descrição de onde foi realizado os testes]

## Reproduzido em:
[Informar detalhadamente caso o teste tenha sido reproduzido em algum outro ambiente.]

## Evidências:
[Quaisquer evidências que possam ajudar a entender e resolver o problema.]

## Impacto do Problema:
[Descrever o impacto que o problema está causando, seja ele crítico, de baixa prioridade ou apenas uma melhoria.]

## Observações Adicionais:
[Fornecer quaisquer observações adicionais relevantes que possam ajudar na resolução do problema.]
